module Types exposing (..)

import Keyboard exposing (KeyCode)
import Time exposing (Time)

type alias Vector = (Int, Int)

type alias State =
  { fps: Float
  , score: Int
  , size: Vector
  , head: Vector
  , tail: List Vector
  , direction: Vector
  , foodPos: Maybe Vector }

type Msg
  = KeyMsg KeyCode
  | Tick Time
  | Spawn Vector
  | ChangeFps String
  | ChangeWidth String
  | ChangeHeight String